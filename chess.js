function display(){
 
    var mainDiv = document.createElement('div');
	mainDiv.style.width = '1200px';
	mainDiv.style.margin = '0 auto';
	mainDiv.style.display = 'grid';
	mainDiv.style.gridTemplateColumns = 'repeat(10, 1fr)';
	mainDiv.style.gridTemplateRows = 'repeat(10, 1fr)';
	mainDiv.style.gridGap = '3px';
	
	mainDiv.style.justifyContent = 'center';
	document.getElementsByTagName('body')[0].appendChild(mainDiv);

	var bWhiteM = ['&#9820', '&#9822', '&#9821', '&#9819', '&#9818', '&#9821', '&#9822', '&#9820','&#9817'];
	var wWhiteM = ['&#9814', '&#9816', '&#9815', '&#9813', '&#9812', '&#9815', '&#9816', '&#9814','&#9823'];
	
	var divCub;
	var stri;
	for(var i=0;i<100;i++){
		divCub = document.createElement('div');
		divCub.style.placeContent = 'center';
		//divCub.innerText = i;//'&#9817';
		divCub.style.fontWeight = 'bold';
		divCub.style.fontSize = '60pt';
		stri = ''+i;
 		if ((i < 10) || (i > 89) || (stri[1]==0) || (stri[1]==9)) divCub.style.background = 'white'
			else if ((parseInt(stri[0])+parseInt(stri[1]))%2>0) divCub.style.background = 'gray'
				else divCub.style.background = 'white';
		mainDiv.appendChild(divCub);
		divCub.style.height = '100px';
		
		//разметка
		if (i==1) divCub.innerText = 'H';
		if (i==2) divCub.innerText = 'G';
		if (i==3) divCub.innerText = 'F';
		if (i==4) divCub.innerText = 'E';
		if (i==5) divCub.innerText = 'D';
		if (i==6) divCub.innerText = 'C';	
		if (i==7) divCub.innerText = 'B';
		if (i==8) divCub.innerText = 'A';		
		
		if (i==10) divCub.innerText = 8;
		if (i==20) divCub.innerText = 7;
		if (i==30) divCub.innerText = 6;
		if (i==40) divCub.innerText = 5;
		if (i==50) divCub.innerText = 4;
		if (i==60) divCub.innerText = 3;	
		if (i==70) divCub.innerText = 2;
		if (i==80) divCub.innerText = 1;
		
		if (i==19) divCub.innerText = 8;
		if (i==29) divCub.innerText = 7;
		if (i==39) divCub.innerText = 6;
		if (i==49) divCub.innerText = 5;
		if (i==59) divCub.innerText = 4;
		if (i==69) divCub.innerText = 3;	
		if (i==79) divCub.innerText = 2;
		if (i==89) divCub.innerText = 1;	

		if (i==91) divCub.innerText = 'A';
		if (i==92) divCub.innerText = 'B';
		if (i==93) divCub.innerText = 'C';
		if (i==94) divCub.innerText = 'D';
		if (i==95) divCub.innerText = 'E';
		if (i==96) divCub.innerText = 'F';	
		if (i==97) divCub.innerText = 'G';
		if (i==98) divCub.innerText = 'H';	

		/*switch (i) {
			case 19: 
				divCub.innerHTML = 8;
				break;
			case 29: 
				divCub.innerHTML = 7;
				break;
			case 39: 
				divCub.innerHTML = 6;
				break;
			case 49: 
				divCub.innerHTML = 5;
				break;
			case 59: 
				divCub.innerHTML = 4;
				break;								
			case 69: 
				divCub.innerHTML = 3;
				break;								
			case 79: 
				divCub.innerHTML = 2;
				break;								
			case 89: 
				divCub.innerHTML = 1;
				break;								
		}*/
		
		if ((stri[1]==9) || (i<9))
			divCub.style.transform = 'rotate(180deg)';
		
		//фигуры
		if ((i > 20) && (i < 29)) 
			divCub.innerHTML = wWhiteM[8];
		if ((i > 70) && (i < 79)) 
			divCub.innerHTML = bWhiteM[8];
		
		if ((i > 10) && (i < 19)) 
			divCub.innerHTML = bWhiteM[stri[1]-1];
		
		if ((i > 80) && (i < 89)) 
			divCub.innerHTML = wWhiteM[stri[1]-1];		
		
	}
}

display()
